(() => {
    var listen = R.curry((type, el) => Kefir.fromEvents(el, type));

    var $ = sel => IO.of(R.head(document.querySelectorAll(sel)));

    var getAttr = R.curry((attr, el) => el.getAttribute(attr));

    var setHtml = R.curry((html, el) => {
        el.innerHTML = html; return el; 
    });

    var model = (() => {
        var stack = [], display = '';
        return {
            moveToStack: () => {
                stack.push(Number(display));
                display = '';
            },
            clearAll: () => {
                stack = []; 
                display = '';
            },
            applyFn: fn => {
                stack.push(Number(display));
                display = stack.reduce(fn);
                stack = [];
            },
            updateDisplay: val => display += val,
            getStack: () => stack,
            getDisplay: () => display
        }
    }());

    var keypadClickStream = R.map(listen('click'), $('#keypad'));

    var keyupStream = listen('keyup', window);

    var getDataValue = R.compose(getAttr('data-value'), R.prop('target'));

    var updateScreen = () => {
        R.map(setHtml(model.getDisplay()), $('#display')).runIO();
        R.map(setHtml(model.getStack().join(', ')), $('#stack')).runIO();
    };

    var moveToStack = () => {
        model.moveToStack();
        updateScreen();
    };

    var clearAll = () => {
        model.clearAll();
        updateScreen();
    };

    var applyFn = op => {
        model.applyFn(R[op]);
        updateScreen();
    };

    var updateDisplayData = n => {
        model.updateDisplay(n);
        updateScreen();
    };

    var applyValue = R.cond([
        [R.equals('enter'), moveToStack],
        [R.equals('clear'), clearAll],
        [isNaN, applyFn],
        [R.T, updateDisplayData]
    ]);

    var buttonClickHandler = R.compose(applyValue, getDataValue);

    keypadClickStream
        .runIO()
        .onValue(buttonClickHandler);

}());
